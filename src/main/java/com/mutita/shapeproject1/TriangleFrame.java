/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mutita.shapeproject1;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author Admin
 */
public class TriangleFrame {
    public static void main(String[] args) {
        final JFrame frame = new JFrame("Triangle");
        frame.setSize(300,300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);
        
        JLabel lblbase = new JLabel("Base",JLabel.TRAILING);
        lblbase.setSize(30,20);
        lblbase.setLocation(5,5);
        lblbase.setBackground(Color.WHITE);
        lblbase.setOpaque(true);
        frame.add(lblbase);
      
        final JTextField txtBase = new  JTextField();
        txtBase.setSize(50,20);
        txtBase.setLocation(60,5);
        frame.add(txtBase);
        
        JLabel lblheigth = new JLabel("Heigth",JLabel.LEADING);
        lblheigth.setSize(40,20);
        lblheigth.setLocation(5,30);
        lblheigth.setBackground(Color.WHITE);
        lblheigth.setOpaque(true);
        frame.add(lblheigth);
        
        final JTextField txtHeigth = new  JTextField();
        txtHeigth.setSize(50,20);
        txtHeigth.setLocation(60,30);
        frame.add(txtHeigth);
        
        JButton btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100,20);
        btnCalculate.setLocation(120,5);
        frame.add(btnCalculate);
        
        final JLabel lblResult = new JLabel("Triangle b = ??? h = ??? area = ??? p = ???" );
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setLocation(0,50);
        lblResult.setSize(300,50);
        lblResult.setBackground(Color.MAGENTA);
        lblResult.setOpaque(true);
        frame.add(lblResult);
        
       btnCalculate.addActionListener(new ActionListener(){
          
            @Override
            public void actionPerformed(ActionEvent e) {
            try{
            String strBase = txtBase.getText();
            String strHeigth = txtHeigth.getText();
            double Base = Double.parseDouble(strBase);
            double Heigth = Double.parseDouble(strHeigth);
            Triangle triangle = new Triangle(Base,Heigth);
            lblResult.setText("Triangle b="+String.format("%.2f",triangle.getBase())
                                +" h = " +String.format("%.2f",triangle.getHeigth())
                                +" area = " +String.format("%.2f",triangle.calArea())
                                +" p = " +String.format("%.2f",triangle.calPerimeter()));
            }catch(Exception ex){
                JOptionPane.showMessageDialog(frame,"Error Please input number",
                        "Error",JOptionPane.ERROR_MESSAGE);
                txtBase.setText("");
                txtBase.requestFocus();
                txtHeigth.setText("");
                txtHeigth.requestFocus();
            }
         }
        });
                
                
                
        
        frame.setVisible(true);
    
    }
}
