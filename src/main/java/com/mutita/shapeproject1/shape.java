/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mutita.shapeproject1;

/**
 *
 * @author Admin
 */
public abstract class shape {
    private String shapeName;
    
    public shape (String shapeName){
        this.shapeName=shapeName;
    }

    public String getShapeName() {
        return shapeName;
    }
    
    public abstract double calArea();
    public abstract double calPerimeter();
    
}
