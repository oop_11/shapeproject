/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mutita.shapeproject1;

/**
 *
 * @author Admin
 */
public class Circle extends shape {
    
    private double Radius;

    public Circle(double Radius) {
        super("Circle");
        this.Radius=Radius;
    }

    public double getRadius() {
        return Radius;
    }

    public void setRadius(double Radius) {
        this.Radius = Radius;
    }

    @Override
    public double calArea() {
       return Math.PI*Math.pow(Radius, 2);
    }

    @Override
    public double calPerimeter() {
       return 0.5*Math.PI*Radius;  
    }
    
}
