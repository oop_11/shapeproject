/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mutita.shapeproject1;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author Admin
 */
public class RectangleFrame {
    public static void main(String[] args) {
        final JFrame frame = new JFrame("Rectangle");
        frame.setSize(300,300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);
        
        JLabel lblwidth = new JLabel("Width",JLabel.LEADING);
        lblwidth.setSize(40,20);
        lblwidth.setLocation(5,5);
        lblwidth.setBackground(Color.WHITE);
        lblwidth.setOpaque(true);
        frame.add(lblwidth);
      
        final JTextField txtwidth = new  JTextField();
        txtwidth.setSize(50,20);
        txtwidth.setLocation(60,5);
        frame.add(txtwidth);
        
        JLabel lblheigth = new JLabel("Heigth",JLabel.LEADING);
        lblheigth.setSize(40,20);
        lblheigth.setLocation(5,30);
        lblheigth.setBackground(Color.WHITE);
        lblheigth.setOpaque(true);
        frame.add(lblheigth);
        
        final JTextField txtHeigth = new  JTextField();
        txtHeigth.setSize(50,20);
        txtHeigth.setLocation(60,30);
        frame.add(txtHeigth);
        
        JButton btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100,20);
        btnCalculate.setLocation(120,5);
        frame.add(btnCalculate);
        
        final JLabel lblResult = new JLabel("Rectangle w = ??? h = ??? area = ??? perimeter = ??? ");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setLocation(0,50);
        lblResult.setSize(300,50);
        lblResult.setBackground(Color.MAGENTA);
        lblResult.setOpaque(true);
        frame.add(lblResult);
        
       btnCalculate.addActionListener(new ActionListener(){
          
            @Override
            public void actionPerformed(ActionEvent e) {
            try{
            String strWidth = txtwidth.getText();
            String strHeigth = txtHeigth.getText();
            double Width = Double.parseDouble(strWidth);
            double Heigth = Double.parseDouble(strHeigth);
            Rectangle rectangle = new Rectangle(Width,Heigth);
            lblResult.setText("Rectangle w="+String.format("%.2f",rectangle.getWidth())
                                +" h= " +String.format("%.2f",rectangle.getHeigth())
                                +" area= " +String.format("%.2f",rectangle.calArea())
                                +" p= " +String.format("%.2f",rectangle.calPerimeter()));
            }catch(Exception ex){
                JOptionPane.showMessageDialog(frame,"Error Please input number",
                        "Error",JOptionPane.ERROR_MESSAGE);
                txtwidth.setText("");
                txtwidth.requestFocus();
                txtHeigth.setText("");
                txtHeigth.requestFocus();
            }
            }
         
        });
       frame.setVisible(true);
    }
}
