/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mutita.shapeproject1;

/**
 *
 * @author Admin
 */
public class Triangle extends shape {
    private double base;
    private double heigth;

    public Triangle(double base,double heigth) {
        super("Triangle");
        this.base=base;
        this.heigth=heigth;
    }

    public double getBase() {
        return base;
    }

    public void setBase(double base) {
        this.base = base;
    }

    public double getHeigth() {
        return heigth;
    }

    public void setHeigth(double heigth) {
        this.heigth = heigth;
    }

    @Override
    public double calArea() {
        return 0.5*heigth*base;
    }

    @Override
    public double calPerimeter() {
        double c = 0;
        c=Math.pow(heigth,2)+Math.pow(base,2);
        return (Math.sqrt(c))+heigth+base;
    }

    
    
}
