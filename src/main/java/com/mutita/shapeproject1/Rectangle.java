/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mutita.shapeproject1;

/**
 *
 * @author Admin
 */
public class Rectangle extends shape{
    private double width;
    private double heigth;
    
    public Rectangle(double width,double heigth) {
        super("Rectangle");
        this.heigth=heigth;
        this.width=width;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getHeigth() {
        return heigth;
    }

    public void setHeigth(double heigth) {
        this.heigth = heigth;
    }

    @Override
    public double calArea() {
       return width*heigth;
    }

    @Override
    public double calPerimeter() {
        return 2.0*(width+heigth);
    }
    
}
